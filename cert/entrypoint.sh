#!/bin/sh
# docker entrypoint script
# generate three tier certificate chain

SUBJ="/C=$COUNTY/ST=$STATE/L=$LOCATION/O=$ORGANISATION"

if [ ! -f "$ROOT_NAME.crt" ]
then
  # generate root certificate
  ROOT_SUBJ="$SUBJ/CN=$ROOT_CN"

  openssl genrsa \
    -out "$ROOT_NAME.key" \
    "$RSA_KEY_NUMBITS"

  openssl req \
    -new \
    -key "$ROOT_NAME.key" \
    -out "$ROOT_NAME.csr" \
    -subj "$ROOT_SUBJ"

  openssl req \
    -x509 \
    -key "$ROOT_NAME.key" \
    -in "$ROOT_NAME.csr" \
    -out "$ROOT_NAME.crt" \
    -days "$DAYS" \
    -subj "$ROOT_SUBJ"

else
  echo "ENTRYPOINT: $ROOT_NAME.crt already exists"
fi

if [ ! -f "$PUBLIC_NAME.key" ]
then
  # generate public rsa key
  openssl genrsa \
    -out "$PUBLIC_NAME.key" \
    "$RSA_KEY_NUMBITS"
else
  echo "ENTRYPOINT: $PUBLIC_NAME.key already exists"
fi

if [ ! -f "$PUBLIC_NAME.crt" ]
then
  # generate public certificate
  PUBLIC_SUBJ="$SUBJ/CN=$PUBLIC_CN"
  openssl req \
    -new \
    -key "$PUBLIC_NAME.key" \
    -out "$PUBLIC_NAME.csr" \
    -subj "$PUBLIC_SUBJ"

  # append public cn to subject alt names
  echo -e "extendedKeyUsage = serverAuth,clientAuth\n" \
          "subjectAltName = @alt_names\n" \
          "[alt_names]\n" \
          "DNS.1 = $PUBLIC_CN" > /tmp/public.ext
  let pos=1
  for name in ${ALT_NAMES//,/ }
  do
    let pos+=1
    echo "DNS.$pos = $name" >> /tmp/public.ext
  done

  cat /tmp/public.ext

  openssl x509 \
    -req \
    -in "$PUBLIC_NAME.csr" \
    -CA "$ROOT_NAME.crt" \
    -CAkey "$ROOT_NAME.key" \
    -out "$PUBLIC_NAME.crt" \
    -CAcreateserial \
    -extfile /tmp/public.ext \
    -days "$DAYS"

else
  echo "ENTRYPOINT: $PUBLIC_NAME.crt already exists"
fi

if [ ! -f "pem/ca.pem" ]
then
    mkdir pem
fi

if [ ! -f "pem/ca.pem" ]
then
  cat "$ROOT_NAME.crt" > "pem/ca.pem"
else
  echo "ENTRYPOINT: ca.pem already exists"
fi

if [ ! -f "pem/$PUBLIC_NAME.pem" ]
then
  # make combined root and issuer ca.pem
  cat "$PUBLIC_NAME.key" "$PUBLIC_NAME.crt" "$ROOT_NAME.crt" > "pem/$PUBLIC_NAME.pem"
else
  echo "ENTRYPOINT: $PUBLIC_NAME.pem already exists"
fi
