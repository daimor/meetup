// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html

let seleniumHost = process.env.SELENIUM_HOST || 'chrome-selenium';

module.exports = function (config) {
  // Get local IP for selenium runner
  var address,
    ifaces = require('os').networkInterfaces();
  for (var dev in ifaces) {
    ifaces[dev].filter((details) => details.family === 'IPv4' && details.internal === false ? address = details.address : undefined);
  }

  config.set({
    hostname: address,
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-webdriver-launcher'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('karma-minimal-reporter'),
      require('@angular/cli/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    files: [
      { pattern: './src/test.ts', watched: false }
    ],
    preprocessors: {
      './src/test.ts': ['@angular/cli']
    },
    mime: {
      'text/x-typescript': ['ts', 'tsx']
    },
    coverageIstanbulReporter: {
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: config.angularCli && config.angularCli.codeCoverage
      ? ['minimal', 'coverage-istanbul']
      : ['minimal', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: [
      'Chrome'
    ],
    customLaunchers: {
      ChromeDebugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=9333']
      },
      ChromeSelenium: {
        base: 'WebDriver',
        config: {
          hostname: seleniumHost,
          port: 4444
        },
        browserName: 'chrome',
        name: 'Karma',
        pseudoActivityInterval: 30000
      }
    },
    singleRun: false
  });
};
