import { Injectable, ReflectiveInjector } from '@angular/core';
import { TestBed, async, fakeAsync, tick, inject, flush } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Router, ActivatedRoute, Params } from '@angular/router';

import { AuthService, AuthInterceptor } from './auth.service';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http/src/params';
import { Observable } from 'rxjs/Observable';
import { TestRequest } from '@angular/common/http/testing/src/request';

@Injectable()
export class ActivatedRouteStub {
  private subject = new BehaviorSubject(this.testParams);
  params = this.subject.asObservable();

  private _testParams: {};
  get testParams() { return this._testParams; }
  set testParams(params: {}) {
    this._testParams = params;
    this.subject.next(params);
  }
}

class MockRouter {
  navigateByUrl(url: string, options?: any) { return url; }
  navigate(url: string[], options?: any) { return url; }
}

describe('Service: Auth', () => {

  let authService: AuthService;

  let http: HttpClient;

  let httpMock: HttpTestingController;

  let accessToken: string;

  let mockActivatedRoute: ActivatedRouteStub;

  beforeEach(() => {
    mockActivatedRoute = new ActivatedRouteStub();

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: Router, useValue: new MockRouter() },
        { provide: ActivatedRoute, useValue: mockActivatedRoute },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true,
        },
        AuthService,
      ]
    });
    authService = TestBed.get(AuthService);
    http = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should ...', () => {
    expect(authService).toBeTruthy();
  });
});
