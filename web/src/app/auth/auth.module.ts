import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RequestOptions } from '@angular/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './login/logout.component';
import { AuthService, AuthGuardService, AuthInterceptor } from './auth.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  ],
  declarations: [
    LoginComponent,
    LogoutComponent,
  ],
  exports: [
    LoginComponent,
    LogoutComponent,
  ],
  providers: [
    AuthInterceptor,
  ]
})
export class AuthModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        AuthGuardService,
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
      ]
    };
  }
}
