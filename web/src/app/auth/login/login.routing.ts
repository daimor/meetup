import { Routes } from '@angular/router';
import { AuthService, AuthGuardService } from '../auth.service';
import { LoginComponent } from './login.component';
import { LogoutComponent } from './logout.component';

export const loginRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent }
];
export const authProviders = [
  AuthGuardService,
  AuthService
];
