import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TestBed, async, inject } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

import { of } from 'rxjs/observable/of';
import { HttpClientTestingModule } from '@angular/common/http/testing';

class MockRouter {
  navigate = jasmine.createSpy('navigate');
}

describe('Component: Login', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports: [
        FormsModule,
        HttpClientTestingModule
      ],
      providers: [
        AuthService,
        { provide: Router, useValue: new MockRouter() },
        { provide: ActivatedRoute, useValue: { params: of({}) } },
      ],
    });
  });

  it('should create an instance', async(() => {
    let fixture = TestBed.createComponent(LoginComponent);
    let component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
