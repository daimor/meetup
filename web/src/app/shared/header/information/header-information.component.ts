import {
  Component,
  OnInit,
  Input,
} from '@angular/core';

@Component({
  selector: 'app-headerinformation',
  templateUrl: './header-information.component.html',
  styleUrls: ['./header-information.component.scss']
})
export class HeaderInformationComponent implements OnInit {
  @Input () data: any;
  collapsed: boolean = true;
  constructor() { }

  ngOnInit() {
  }

  changeCollapse() {
    this.collapsed = !this.collapsed;
  }

  getTooltip() {
    if (this.collapsed) {
      return 'Zobrazit informace';
    } else {
      return 'Skrýt informace';
    }
  }

}
