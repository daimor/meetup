import { Utils } from './';

describe('Utils', () => {

  it('UpperCamelCase', () => {
    expect(Utils.toUpperCamelCase('test')).toEqual('Test');
    expect(Utils.toUpperCamelCase('testCase')).toEqual('TestCase');
  });

});
