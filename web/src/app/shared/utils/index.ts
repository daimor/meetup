import { NgModuleFactory } from '@angular/core';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Subject } from 'rxjs/Subject';


export * from './components-helper.service';

import * as ObjectPath from 'object-path';
/*
* ObjectPath methods from https://github.com/mariocasciaro/object-path
*/

export class Utils {

  static toUpperCamelCase(string: string) {
    return string.replace(/(?:^|\s)(\w)/g, function (matches, letter) {
      return letter.toUpperCase();
    });
  }

  static objectPathSet(obj: any, path: any, value: any, doNotReplace = false) {
    return ObjectPath.set(obj, path, value, doNotReplace);
  }

  static objectPathGet(obj, path, defaultValue?: any) {
    return ObjectPath.get(obj, path, defaultValue);
  }
}

/**
 * Determine if the argument is shaped like a Promise
 */
export function isPromise(obj: any): obj is Promise<any> {
  // allow any Promise/A+ compliant thenable.
  // It's up to the caller to ensure that obj.then conforms to the spec
  return !!obj && typeof obj.then === 'function';
}

/**
 * Determine if the argument is an Observable
 */
export function isObservable(obj: any | Observable<any>): obj is Observable<any> {
  // TODO use Symbol.observable when https://github.com/ReactiveX/rxjs/issues/2415 will be resolved
  return !!obj && typeof obj.subscribe === 'function';
}

export function wrapIntoObservable<T>(value: T | NgModuleFactory<T> | Promise<T> | Observable<T>):
  Observable<T> {
  if (isObservable(value)) {
    return value;
  }

  if (isPromise(value)) {
    // Use `Promise.resolve()` to wrap promise-like instances.
    // Required ie when a Resolver returns a AngularJS `$q` promise to correctly trigger the
    // change detection.
    return fromPromise(Promise.resolve(value));
  }

  return of(value as T);
}

export function componentDestroyed(component: { ngOnDestroy(): void; }): Observable<void|undefined|{}> {
  const oldNgOnDestroy = component.ngOnDestroy;
  const stop$ = new Subject();
  component.ngOnDestroy = function () {
    if (oldNgOnDestroy) { oldNgOnDestroy.apply(component); }
    stop$.next(undefined);
    stop$.complete();
  };
  return stop$;
}
