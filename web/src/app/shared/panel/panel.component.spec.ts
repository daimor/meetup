/* tslint:disable:no-unused-variable */

import { By } from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';
import { TestBed, async, inject } from '@angular/core/testing';
import { PanelComponent } from './panel.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('Component: Panel', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
        ])
      ],
      declarations: [
        PanelComponent
      ],
    });
  });

  it('should create an instance', async(() => {
    let fixture = TestBed.createComponent(PanelComponent);
    let component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
