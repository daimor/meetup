import { Component, OnInit, Input, Output, ElementRef, EventEmitter, TemplateRef, HostBinding } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  @HostBinding('title') htmlTitle = '';

  @Input() title: string;

  @Input() canCollapse: boolean;

  @Input() actions: TemplateRef<any>;

  @Input() back: any;

  @Output() scroll: EventEmitter<any> = new EventEmitter();

  collapsed: boolean = false;

  @Input() canClose: boolean;

  constructor(el: ElementRef) { }

  ngOnInit() {
  }

  collapse(): void {
    this.collapsed = !this.collapsed;
  }

  onScroll(event): void {
    this.scroll.emit(event);
  }

}
