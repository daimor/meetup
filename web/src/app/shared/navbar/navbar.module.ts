import { NavbarService } from './navbar.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NavbarComponent, NavbarListComponent, CollapsableDirective } from './navbar.component';

import { RouterModule } from '@angular/router';

import { AuthModule } from '@app/auth';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgbModule,
    AuthModule
  ],
  exports: [
    NavbarComponent
  ],
  declarations: [
    NavbarComponent,
    NavbarListComponent,
    CollapsableDirective
  ],
  providers: [
    NavbarService
  ]
})
export class NavbarModule { }
