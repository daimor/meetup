import { NavbarService } from './navbar.service';
import { By } from '@angular/platform-browser';
import { DebugElement, ElementRef } from '@angular/core';
import { TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, ConnectionBackend, BaseRequestOptions } from '@angular/http';
import { NavbarComponent, NavbarListComponent, CollapsableDirective } from './navbar.component';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthModule } from '@app/auth';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Component: Navbar', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavbarComponent,
        NavbarListComponent,
        CollapsableDirective
      ],
      imports: [
        HttpClientTestingModule,
        AuthModule,
        RouterTestingModule.withRoutes([
        ])
      ],
      providers: [
        NavbarService,
      ]
    });
  });

  it('should create an instance', async(() => {
    let fixture = TestBed.createComponent(NavbarComponent);
    let component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
