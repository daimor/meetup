import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { PanelComponent } from './panel';

import { HeaderButtonsComponent } from './header/buttons/header-buttons.component';
import { HeaderInformationComponent } from './header/information/header-information.component';

import { NavbarService } from './navbar/navbar.service';

import {
  NgbModule, NgbDropdownModule,
} from '@ng-bootstrap/ng-bootstrap';
import { SidePanelComponent } from './side-panel/side-panel.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgbModule,
  ],
  exports: [
    PanelComponent,
    HeaderButtonsComponent,
    HeaderInformationComponent,
    SidePanelComponent,
  ],
  declarations: [
    PanelComponent,
    HeaderButtonsComponent,
    HeaderInformationComponent,
    HeaderButtonsComponent,
    HeaderInformationComponent,
    SidePanelComponent
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        NavbarService,
      ]
    };
  }

}
