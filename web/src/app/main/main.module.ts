import { NgModule } from '@angular/core';
import { RequestOptions, BaseRequestOptions, RequestOptionsArgs } from '@angular/http';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';

import {
  SharedModule,
  NavbarModule
} from '@app/shared';
import {
  NgbModule,
  NgbTooltipConfig,
  NgbDropdownConfig,
  NgbTabsetConfig
} from '@ng-bootstrap/ng-bootstrap';
import { MainRoutingModule } from './main-routing.module';

import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from '@app/auth';
import { NewComponent } from './new/new.component';
import { FormsModule } from '@angular/forms';
import { DetailsComponent } from './details/details.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MainRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
    AuthModule,
    NavbarModule
  ],
  declarations: [
    MainComponent,
    HomeComponent,
    NewComponent,
    DetailsComponent
  ]
})
export class MainModule { }
