import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { MockBackend, MockConnection } from '@angular/http/testing';
import {
  Http,
  ConnectionBackend,
  BaseRequestOptions,
} from '@angular/http';
import { TestBed, async, inject } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { SharedModule } from '@app/shared';
import { MainModule } from './../main.module';
import { MainRoutingModule } from './../main-routing.module';

import { AuthService } from '@app/auth';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Component: Home', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        SharedModule,
        MainModule,
        RouterTestingModule.withRoutes([
        ])
      ],
      providers: [
        MockBackend,
        AuthService,
      ]
    });
  });

  it('should create an instance', async(() => {
    let fixture = TestBed.createComponent(HomeComponent);
    let component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  }));
});
