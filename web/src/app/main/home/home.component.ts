import { Component, HostBinding, OnInit } from '@angular/core';
import { environment } from '@env/environment';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @HostBinding('class') class = 'flex-column';

  title: string = 'Home';

  notes: any[];

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.loadNotes();
  }

  loadNotes() {
    this.http.get<any[]>('/api/demoapp/notes').subscribe(data => {
      this.notes = data;
    });
  }

  deleteNote(id) {
    this.http
      .delete(`/api/demoapp/notes/${id}`)
      .catch(error => {
        alert(error);
        throw error;
      })
      .subscribe(data => {
        this.loadNotes();
      });
  }
}
