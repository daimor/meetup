import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../auth/';

import { MainComponent } from './main.component';
import { HomeComponent } from './home/home.component';
import { NewComponent } from './new/new.component';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuardService],
    children: [{
      path: '',
      children: [{
        path: 'home',
        component: HomeComponent
      }, {
        path: 'new',
        component: NewComponent
      }, {
        path: 'details/:id',
        component: DetailsComponent
      }, {
        path: '',
        redirectTo: 'home',
        pathMatch: 'prefix'
      }]
    }]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
