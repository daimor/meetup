import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  formData: any;

  constructor(private http: HttpClient, private router: Router) {
    this.formData = {
      title: '',
      text: ''
    };
  }

  ngOnInit() {}

  save() {
    this.http.put('/api/demoapp/notes', this.formData)
    .catch(error => {
      alert(error.message || error);
      throw error;
    })
    .subscribe(data => {
      console.log('save result', data);
      this.router.navigate(['home']);
    });
    return false;
  }
}
