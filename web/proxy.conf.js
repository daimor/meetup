var demoappServer = process.env.DEMOAPP_SERVER || 'http://server:52773/';

const PROXY_CONFIG = {
  "/api/demoapp": {
    "target": demoappServer,
    "secure": false,
    "changeOrigin": true,
    "pathRewrite": {
      "^/api/demoapp": ""
    }
  },
}

module.exports = PROXY_CONFIG;
