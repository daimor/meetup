Class App.Installer
{

XData MyInstall [ XMLNamespace = INSTALLER ]
{
<Manifest>
  <Default Name="NAMESPACE" Value="DEMOAPP"/>
  <Default Name="DBNAME" Value="DEMOAPP"/>
  <Default Name="APPPATH" Dir="/opt/app/" />
  <Default Name="SOURCESPATH" Dir="${APPPATH}src" />
  <Default Name="CSPAPP" Value="/api/demoapp/" />
  <Default Name="CSPAPPDIR" Dir="${APPPATH}/csp" />

  <Namespace Name="%SYS">
    <Invoke Class="Security.Users" Method="Import" CheckStatus="true">
      <Arg Value="${APPPATH}/Users.xml"/>
    </Invoke>
  </Namespace>

  <Namespace Name="${NAMESPACE}" Code="${DBNAME}" Data="${DBNAME}" Create="yes" Ensemble="0">
    <Configuration>
        <Database Name="${DBNAME}" Dir="${MGRDIR}/${DBNAME}" Create="yes"/>
    </Configuration>

    <CSPApplication Url="${CSPAPP}"
      Directory="${CSPAPPDIR}"
      AuthenticationMethods="32"/>

    <Import File="${SOURCESPATH}" Recurse="1"/>
  </Namespace>

  <Namespace Name="${CURRENTNS}">
    <Invoke Class="${CURRENTCLASS}" Method="SetDispatchClass" CheckStatus="true">
      <Arg Value="${CSPAPP}"/>
      <Arg Value="App.REST"/>
    </Invoke>
  </Namespace>
</Manifest>
}

ClassMethod setup(ByRef pVars, pLogLevel As %Integer = 3, pInstaller As %Installer.Installer, pLogger As %Installer.AbstractLogger) As %Status [ CodeMode = objectgenerator, Internal ]
{
  do %code.WriteLine($char(9)_"set pVars(""CURRENTCLASS"")="""_%classname_"""")
  do %code.WriteLine($char(9)_"set pVars(""CURRENTNS"")="""_$namespace_"""")
  Quit ##class(%Installer.Manifest).%Generate(%compiledclass, %code, "MyInstall")
}

ClassMethod SetDispatchClass(pCSPName As %String = "", pDispatchClass As %String = "") As %Status
{
    new $namespace
    znspace "%SYS"
    set props("DispatchClass")=pDispatchClass
    set props("Recurse")=1
    d ##class(Security.Applications).Modify(pCSPName,.props)
    quit $$$OK
}

}
