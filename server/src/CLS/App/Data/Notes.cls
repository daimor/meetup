Class App.Data.Notes Extends %Persistent
{

Property TimeStamp As %TimeStamp;

Property Title As %String(MAXLEN = 50);

Property Author As %String;

Property Text As %String(MAXLEN = 200);

Query List(User As %String) As %SQLQuery
{
SELECT ID, Title, Text, TimeStamp
FROM Notes
WHERE :User IS NULL OR Author = :User
}

Storage Default
{
<Data name="NotesDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>TimeStamp</Value>
</Value>
<Value name="3">
<Value>Title</Value>
</Value>
<Value name="4">
<Value>Author</Value>
</Value>
<Value name="5">
<Value>Text</Value>
</Value>
</Data>
<DataLocation>^App.Data.NotesD</DataLocation>
<DefaultData>NotesDefaultData</DefaultData>
<IdLocation>^App.Data.NotesD</IdLocation>
<IndexLocation>^App.Data.NotesI</IndexLocation>
<StreamLocation>^App.Data.NotesS</StreamLocation>
<Type>%Storage.Persistent</Type>
}

}
